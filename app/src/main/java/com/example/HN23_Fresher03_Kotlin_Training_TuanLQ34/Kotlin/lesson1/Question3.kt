package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson1

fun main() {
    println("Input number of element: ")
    var input = Integer.valueOf(readlnOrNull())
    var array = IntArray(input)
    for (n in 0 until input) {
        print("a[$n] = ")
        array[n] = Integer.valueOf(readlnOrNull())
    }
    array.sort()
    print("Sorted array: ")
    for (element in array) {
        print("$element, ")
    }

}