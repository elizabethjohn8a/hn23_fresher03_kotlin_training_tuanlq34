package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson1

fun main() {
    print("Enter month: ")
    val month = Integer.valueOf(readlnOrNull())
    print("Enter year: ")
    val year = Integer.valueOf(readlnOrNull())
    val boolean = year % 4 == 0 && year % 100 != 0 || year % 400 == 0
    val output = when (month) {
        1, 3, 5, 7, 8, 10, 12 -> "31 days"
        4, 6, 9, 11 -> "30 days"
        2 -> if (boolean) "29 days" else "28 days"
        else -> "Invalid month!"
    }
    print("Month number $month has $output.")
}