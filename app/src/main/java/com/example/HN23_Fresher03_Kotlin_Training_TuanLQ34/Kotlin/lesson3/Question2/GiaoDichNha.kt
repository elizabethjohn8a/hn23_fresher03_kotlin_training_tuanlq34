package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson3.Question2

import java.util.Date

class GiaoDichNha(
    var maGiaoDich: String = "",
    var ngayGiaoDich: Date = Date(),
    var donGia: Int = 0,
    var loaiNha: String = "",
    var diaChi: String = "",
    var dienTich: Int = 0,
) {
    var thanhTien: Int = 0
        set(value) {
            if (loaiNha == "Thuong gia") {
                field = (dienTich * donGia * 1.1).toInt()
            }
            if (loaiNha == "Cao cap") {
                field = dienTich * donGia
            }
            if (loaiNha == "Thuong") {
                field = (dienTich * donGia * 0.9).toInt()
            }
        }
}