package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson2

fun findPrime(number1: Int, number2: Int) {


    for (i in number1 until number2) {
        var check = true
        if (i > 1) {

            for (j in 2 until i) {
                if (i % j == 0) check = false
            }
        }
        if (check) print("$i, ")
    }

}

//Higher Order Function
fun getFindPrime(): (Int, Int) -> Unit {
    return ::findPrime
}

//Lambda
val lambdaFindPrime = { number1: Int, number2: Int ->
    {


        for (i in number1 until number2) {
            var check = true
            if (i > 1) {

                for (j in 2 until i) {
                    if (i % j == 0) check = false
                }
            }
            if (check) print("$i, ")
        }

    }
}

fun main() {
    findPrime(10, 20)
    val getFindPrimeClone = getFindPrime()
    getFindPrimeClone(10, 20)
    val lambdaFindPrimeCLone = lambdaFindPrime
    lambdaFindPrimeCLone(10, 20)
}
