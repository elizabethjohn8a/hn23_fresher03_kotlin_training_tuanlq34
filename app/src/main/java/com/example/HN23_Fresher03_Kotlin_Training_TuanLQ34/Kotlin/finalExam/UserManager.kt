package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.finalExam

import java.util.ArrayList

class UserManager(){
    private var userArrayList = ArrayList<User>()
    fun addUser(user: User) {
        userArrayList.add(user)
    }

    fun displayUser() {
        for (user in userArrayList) {
            val (name, age, address) = user
            println("Name: $name, age: $age, address: $address")
        }
    }

    fun findUserByName(name: String) {
        for (user in userArrayList) {
            if (user.name == name) {
                val (name, age, address) = user
                print("User found: ")
                println("Name: $name, age: $age, address: $address")
            }
        }
    }

    val lambdaSorter = {
        ->
        {
            userArrayList.sortedBy { it.name }
            println("Sorted successfully!")
        }
    }

}