package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson4.Question5

fun plusLongNumbers(number1: Long, number2: Long): Long {
    return number1 + number2
}

fun subtractLongNumbers(number1: Long, number2: Long): Long {
    return number1 - number2
}

fun multipleLongNumbers(number1: Long, number2: Long): Long {
    return number1 * number2
}

fun divideLongNumbers(number1: Long, number2: Long): Long {
    return number1 / number2
}