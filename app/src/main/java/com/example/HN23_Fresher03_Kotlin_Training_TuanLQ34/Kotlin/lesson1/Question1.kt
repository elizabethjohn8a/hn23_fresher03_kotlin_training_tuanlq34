package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson1

fun main() {
    print("Result: ")
    for (n in 10..200) {
        if (n % 7 == 0 && n % 5 != 0) {
            print("$n, ")
        }
    }
}