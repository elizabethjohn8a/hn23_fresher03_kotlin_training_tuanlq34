package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson4.Question3

fun <T> findMax(list: List<T>, begin: Int, end: Int, compareFunction: (T) -> Unit) {
    val subList = list.subList(begin, end)
    for (element in subList) {
        compareFunction(element)
    }
}

fun main() {
    var maxInt = 0
    val intList = listOf<Int>(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    findMax(intList, 5, 9) { if (it > maxInt) maxInt = it }

    val stringList = listOf<String>("abc", "bcd", "def", "fgh", "hik")
    var maxString = ""
    findMax(stringList, 1, 3) { if (it > maxString) maxString = it }
    println(maxInt)
    print(maxString)

}