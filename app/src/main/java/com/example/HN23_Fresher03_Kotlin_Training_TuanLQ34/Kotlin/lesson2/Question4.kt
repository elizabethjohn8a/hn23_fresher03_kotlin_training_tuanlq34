package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson2

fun findSumOfDigits(number: String): Int {
    var sum = 0
    for (i in number.indices) {
        sum += number[i].toInt()
    }
    return sum
}

fun getFindSumOfDigits(): (String) -> Int {
    return ::findSumOfDigits
}

val lambda1 = { number: String -> findSumOfDigits(number) }
fun higherOrderFunction1(number: String, calculateSunOfDigits: (number: String) -> Int) {
    calculateSunOfDigits(number)
}

fun main() {
    val lambda2 = lambda1
    lambda2("135")
    higherOrderFunction1(("12345"), getFindSumOfDigits())
}