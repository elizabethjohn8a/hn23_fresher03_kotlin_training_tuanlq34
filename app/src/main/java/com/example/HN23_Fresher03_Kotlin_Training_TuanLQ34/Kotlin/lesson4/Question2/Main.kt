package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson4.Question2

fun <T> swapElement(array: Array<T>, position1: Int, position2: Int) {
    var tempElement: T = array[position1]
    array[position1] = array[position2]
    array[position2] = tempElement


}

fun main() {
    val stringArray = arrayOf("Tuan1", "Tuan2", "Tuan3")
    val intArray = arrayOf(1, 2, 3)
    swapElement(stringArray, 0, 2)
    swapElement(intArray, 1, 2)
    for (element in stringArray) print("$element ")
    print("\n")
    for (element in intArray) print("$element ")
}