package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson1

fun main() {
    println("Input a two-digit integer number: ")
    var input = Integer.valueOf(readlnOrNull())
    if (input in 10..99) {
        val hex = Integer.toHexString(input)
        val bin = Integer.toBinaryString(input)
        print("Hex string : $hex. Binary string: $bin")
    } else println("Please enter a two-digit integer number!")

}