package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.finalExam

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.ArrayList
fun main() {
    var user1 = User("Tuan1", 22, "TuanLQ34.1@fpt.com")
    var user2 = User("Tuan2", 23, "TuanLQ34.2@fpt.com")
    var user3 = User("Tuan3", 23, "TuanLQ34.3@fpt.com")
    val userManager = UserManager()

    //Coroutine simulates adding 3 users in userManager
    var globalScopeJob = GlobalScope.launch {
        userManager.addUser(user1)
        val randomDelay = (500..2000).random().toLong()
        delay(randomDelay)
        userManager.addUser(user2)
        val randomDelay1 = (500..2000).random().toLong()
        delay(randomDelay1)
        userManager.addUser(user3)
        val randomDelay2 = (500..2000).random().toLong()
        delay(randomDelay2)
    }
    runBlocking {
        globalScopeJob.join()
    }
    //Display user list
    userManager.displayUser()
    //Find user "Tuan2"
    userManager.findUserByName("Tuan2")
    //Sort list
    userManager.lambdaSorter
    //List after sorting
    userManager.displayUser()
}