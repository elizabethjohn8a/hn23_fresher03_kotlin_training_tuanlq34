package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson3.Question1

class Teacher : Person {
    constructor() : super()

    var classTeaching: String = ""
        set(value) {
            if (value[0].equals('G') || value[0].equals('H') || value[0].equals('I')
                || value[0].equals('K') || value[0].equals('L') || value[0].equals('M')
            )
                field = value
        }
    var salaryPerHour: Int? = 0
    var hourOfTeaching: Int? = 0
    override fun inputInfo() {
        super.inputInfo()
        println("Class teaching: ")
        this.classTeaching = readlnOrNull().toString()
        println("Salary per hour: ")
        this.salaryPerHour = readlnOrNull()?.toInt()
        println("Hour of teaching: ")
        this.hourOfTeaching = readlnOrNull()?.toInt()
        println("Address: ")
        this.address = readlnOrNull().toString()
    }

    fun calculateSalary(): Int {
        var salary: Int = 0
        if (classTeaching[0] == 'G' || classTeaching[0] == 'H' || classTeaching[0] == 'I'
            || classTeaching[0] == 'K'
        ) {
            salary = hourOfTeaching!! * salaryPerHour!!
        } else salary = hourOfTeaching!! * salaryPerHour!! + 500000
        return salary
    }
}