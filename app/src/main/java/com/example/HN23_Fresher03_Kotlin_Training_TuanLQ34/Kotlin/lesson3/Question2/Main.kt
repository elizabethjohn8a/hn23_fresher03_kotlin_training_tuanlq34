package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson3.Question2

import java.util.Date

fun main() {
    var giaoDichDat1: GiaoDichDat = GiaoDichDat(
        "A01",
        Date(14, 4, 2023), 1000000, "A", 45
    )
    var giaoDichDat2: GiaoDichDat = GiaoDichDat(
        "A02",
        Date(15, 5, 2023), 1200000, "B", 48
    )
    var giaoDichDatArray = arrayOf(giaoDichDat1, giaoDichDat2)
    print(giaoDichDatArray.toString())

    var giaoDichNha1: GiaoDichNha = GiaoDichNha(
        "N01", Date(14, 4, 2020),
        1000000, "Thuong gia", "Ca Mau", 55
    )
    var giaoDichNha2: GiaoDichNha = GiaoDichNha(
        "N02", Date(14, 5, 2020),
        1200000, "Thuong", "Khanh Hoa", 56
    )
    var giaoDichNhaArray = arrayOf(giaoDichNha1, giaoDichNha2)
    println(giaoDichNhaArray.toString())

    println(
        "Co tong cong: ${giaoDichDatArray.size} giao dich dat " +
                "va ${giaoDichNhaArray.size} giao dich nha"
    )

    var sum = 0
    for (giaoDichDat in giaoDichDatArray) {
        giaoDichDat.thanhTien
        sum += giaoDichDat.thanhTien
    }
    print("Trung binh giao dich dat: ${sum / 2}")

//Gia su thang truoc la thangg 4
    for (giaoDichDat in giaoDichDatArray) {
        if (giaoDichDat.ngayGiaoDich.month == 4) {
            print(giaoDichDat.toString())
        }
    }
    for (giaoDichNha in giaoDichNhaArray) {
        if (giaoDichNha.ngayGiaoDich.month == 4) {
            print(giaoDichNha.toString())
        }
    }
}