package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson4.Question5;

public class JavaClass {
    public static void main(String[] args) {
        Long sum = MyKotlinFunctionKt.plusLongNumbers(1, 2);
        Long subtract = MyKotlinFunctionKt.subtractLongNumbers(1, 2);
        Long mul = MyKotlinFunctionKt.multipleLongNumbers(1, 2);
        Long divide = MyKotlinFunctionKt.divideLongNumbers(1, 2);

    }

}
