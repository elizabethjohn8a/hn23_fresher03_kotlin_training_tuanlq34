package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson2

fun isPrime(number : Int) : Boolean{
    var check = true
    if(number > 1){
        for(i in 2 until number){
            if (number%i==0)
                check = false
        }
    } else check = false
    return check
}
//lambda
val lambdaIsPrime = { number: Int ->
    {
        for (i in 2 until number / 2) {
            if (isPrime(i))
                if (isPrime(number - i))
                    println("$number = $i + ${number - i}")
        }
    }
}

//higher order function
fun getIsPrime(): (Int) -> Boolean{
    return ::isPrime
}
//Higher order function
fun check(number: Int, checkPrime : (Int) -> Boolean){
    for(i in 2 until number/2){
        if(checkPrime(i)){
            if(checkPrime(number-i)){
                println("$number = $i + ${number-i}")}}
    }
}
fun main(){
    check(20, getIsPrime())
    lambdaIsPrime(20)
}