package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson4.Question1


fun <T> countNumberOfElement(
    collection: Collection<T>,
    compareFunction: (T) -> Boolean,
): Int {
    var count = 0
    for (element in collection) {
        if (compareFunction(element))
            count++
    }
    return count
}

fun main() {
    val arrayList1 = arrayListOf(1, 2, 3, 4, 5)
    val count = countNumberOfElement(arrayList1) { it % 2 == 0 }
    print(count)


}
