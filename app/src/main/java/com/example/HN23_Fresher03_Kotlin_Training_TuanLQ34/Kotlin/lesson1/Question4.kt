package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson1

fun main() {
    print("Enter a string: ")
    val inputtedString = readlnOrNull()
    var count = 1
    if (inputtedString != null) {
        for (index in inputtedString.indices) {
            if (inputtedString[index] == ' ') {
                count++
            }
        }
    }
    println("Number of word: $count")
    var tempString = ""
    if (inputtedString?.get(0)!!.isLetter()) {
        for (index in inputtedString.indices) {
            tempString += if (index == 0) {
                inputtedString?.get(0)!!.uppercaseChar()
            } else
                inputtedString?.get(index)
        }
    }
    print("After uppercase: $tempString")
}