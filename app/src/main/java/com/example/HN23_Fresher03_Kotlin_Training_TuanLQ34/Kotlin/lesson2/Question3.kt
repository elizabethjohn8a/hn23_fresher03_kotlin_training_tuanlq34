package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson2


fun findSum(number: Int): Int {
    var sum = 0
    if (number > 0) {
        sum += number
        print("$number, ")
    }
    findSum(number - 1)
    return sum
}

fun higherOrderFunction(number: Int, calculateSum: (number: Int) -> Int) {
    calculateSum(number)
}

fun getFindSum(): (Int) -> Int {
    return ::findSum
}

var lambda = { number: Int -> findSum(number) }

fun main() {
    val lambdaClone = lambda
    lambdaClone(5)
    higherOrderFunction(5, getFindSum())
}

