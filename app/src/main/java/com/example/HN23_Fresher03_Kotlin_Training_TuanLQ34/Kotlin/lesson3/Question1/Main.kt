package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson3.Question1

import kotlin.system.exitProcess


fun main() {
    print(
        "1. Add student/teacher \n2. Edit student by id\n" +
                "3. Delete teacher by name\n 4. Sort student by name\n" +
                "5. Display student list\n 6. Display teacher list\n" +
                "7. Sort and display student by average score\n" +
                "8. Sort and display teacher by salary\n 0. Exit"
    )
    val choice = readlnOrNull()?.toInt()
    var studentArray = arrayOfNulls<Student>(3)
    var teacherArray = arrayOfNulls<Teacher>(3)
    when (choice) {
        1 -> {
            for (i in 0..2) {
                var student = Student()
                student.inputInfo()
                studentArray[i] = student
            }
            for (i in 0..2) {
                var teacher = Teacher()
                teacher.inputInfo()
                teacherArray[i] = teacher
            }
        }

        2 -> {
            print("Enter student id: ")
            val id = readlnOrNull()
            for (student in studentArray) {
                if (student?.id == id) student?.inputInfo()
            }
        }

        3 -> {
            print("Enter teacher name: ")
            val name = readlnOrNull()
            val mutableTeacherList = teacherArray.toMutableList()
            for (teacher in teacherArray) {
                if (teacher?.name == name) {
                    mutableTeacherList.remove(teacher)
                    teacherArray.drop(3)
                    for (i in 0..1) {
                        teacherArray[i] = mutableTeacherList[i]
                    }
                }
            }
        }

        4 -> {
            var nameArray = arrayOfNulls<String>(3)
            var i = 0
            for (student in studentArray) {
                nameArray[i] = student?.name
                i++
            }
            print(nameArray.sort())
        }

        5 -> {
            for (student in studentArray)
                println(student.toString())
        }

        6 -> {
            for (teacher in teacherArray)
                println(teacher.toString())
        }

        7 -> {
            var mutableStudentArray = studentArray.toMutableList()
            var studentArray1 = arrayOfNulls<Student>(3)
            var tempStudent: Student = Student()
            for (student in mutableStudentArray) {
                if (student != null) {
                    if (student.averagePoint > tempStudent.averagePoint) {
                        if (student != null) {
                            studentArray1[0] = student
                            mutableStudentArray.remove(student)
                        }
                    }
                }
            }
            for (student in mutableStudentArray) {
                if (student != null) {
                    if (student.averagePoint > tempStudent.averagePoint) {
                        if (student != null) {
                            studentArray1[1] = student
                            mutableStudentArray.remove(student)
                        }
                    }
                }
            }
            studentArray1[2] = mutableStudentArray[0]
            print(mutableStudentArray)
        }

        8 -> {
            var mutableTeacherArray = teacherArray.toMutableList()
            var teacherArray1 = arrayOfNulls<Teacher>(3)
            var tempTeacher: Teacher = Teacher()
            for (teacher in mutableTeacherArray) {
                if (teacher != null) {
                    if (teacher.calculateSalary() > tempTeacher.calculateSalary()) {
                        if (teacher != null) {
                            teacherArray1[0] = teacher
                            mutableTeacherArray.remove(teacher)
                        }
                    }
                }
            }
            for (teacher in mutableTeacherArray) {
                if (teacher != null) {
                    if (teacher.calculateSalary() > tempTeacher.calculateSalary()) {
                        if (teacher != null) {
                            teacherArray1[1] = teacher
                            mutableTeacherArray.remove(teacher)
                        }
                    }
                }
            }
            teacherArray1[2] = mutableTeacherArray[0]
            print(mutableTeacherArray)
        }

        0 -> exitProcess(0)
    }
}

