package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson5.question2

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class InvalidNumberException : Throwable() {
    override fun printStackTrace() {
        print("Catch invalid number exception")
    }

}
class InvalidLengthException : Throwable() {
    override fun printStackTrace() {
        print("Catch InvalidLengthException")
    }
}
class InvalidUsernameException : Throwable() {
    override fun printStackTrace() {
        print("Catch InvalidUsernameException")
    }

}

class InvalidSpecialCharacterException : Throwable() {
    override fun printStackTrace() {
        print("Catch InvalidSpecialCharacterException.")
    }
}

fun main () = runBlocking{
    val handler = CoroutineExceptionHandler { _, invalidUsernameException->
        print("${invalidUsernameException.printStackTrace()}")

    }
    val job = GlobalScope.launch (handler){
        launch {
            try {
                throw InvalidNumberException()
                print("Do not contain number")
            } catch (invalidNumberException : InvalidNumberException){
                invalidNumberException.printStackTrace()
            }

        }
        launch {
            try {
                throw InvalidSpecialCharacterException()
            }catch (invalidSpecialCharacterException : InvalidSpecialCharacterException){
                invalidSpecialCharacterException.printStackTrace()
            }
        }
        launch {
            try {
                throw InvalidLengthException()
            }catch (invalidLengthException : InvalidLengthException){
                invalidLengthException.printStackTrace()
            }
        }
    }
    job.join()
}


