package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson3.Question1

open class Person(
    var name: String = "",
    var gender: String = "",
    var dateOfBirth: String = "",
    var address: String = "",
) {


    constructor (name: String, gender: String, dateOfBirth: String) : this() {
        this.name = name
        this.gender = gender
        this.dateOfBirth = dateOfBirth
    }

    open fun inputInfo() {
        println("Name: ")
        this.name = readlnOrNull().toString()
        println("Gender: ")
        this.gender = readlnOrNull().toString()
        println("Date Of Birth: ")
        this.dateOfBirth = readlnOrNull().toString()
        println("Address: ")
        this.address = readlnOrNull().toString()

    }

    open fun showInfo() {
        print("Name: $name, gender: $gender, date of birth: $dateOfBirth, address: $address")
    }


}