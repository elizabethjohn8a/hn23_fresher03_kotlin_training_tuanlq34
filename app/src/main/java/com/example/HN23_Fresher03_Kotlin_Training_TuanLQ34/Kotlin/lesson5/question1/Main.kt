package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson5.question1


import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


suspend fun fibonacci(number: Int): Int {
    delay(10)
    if (number < 2) {
        return number
    }
    return fibonacci(number - 1) + fibonacci(number - 2)
}

fun main() {


    runBlocking {

        launch {
            delay(3000)
            val result = fibonacci(5)
        }

    }


}