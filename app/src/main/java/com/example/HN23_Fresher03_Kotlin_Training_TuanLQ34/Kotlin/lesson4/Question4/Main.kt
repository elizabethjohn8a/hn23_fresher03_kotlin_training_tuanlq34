package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson4.Question4

class People(
) {

    private var gender: String = ""
        private get() {
            return field
        }
    var age: Int = 0
        get() {
            return field
        }

    val ageReflection = this::age
    val genderReflection = this::gender
    val getAgeReflection = this::age::get
    val getGenderReflection = this::gender::get

}
