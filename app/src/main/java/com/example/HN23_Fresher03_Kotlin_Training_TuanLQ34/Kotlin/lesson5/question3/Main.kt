package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson5.question3

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking

fun integerSum(): Flow<Int> = flow {
    for (i in 0..10) {
        delay(100)
        emit(i)
    }
}

fun main()  {
    runBlocking {
        var sum = 0
        println("Primitive sum: $sum")
        integerSum().collect { value ->
            run {
                println("Current number: $value")
                println("Current sum: ${sum + value}")
                sum += value
            }
        }
    }
}