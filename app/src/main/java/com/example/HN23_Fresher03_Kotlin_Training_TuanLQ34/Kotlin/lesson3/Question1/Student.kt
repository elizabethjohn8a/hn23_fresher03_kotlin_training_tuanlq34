package com.example.HN23_Fresher03_Kotlin_Training_TuanLQ34.Kotlin.lesson3.Question1

const val PREFIX = "SV"

class Student : Person {
    constructor() : super()

    var id: String = ""
        get() {
            return field.trim()
        }
        set(value) {
            field = PREFIX + value
        }
    var averagePoint: Float = 0.0F
        set(value) {
            field = if (value < 0 || value > 10)
                0.0f
            else value
        }

    var email: String = ""
        set(value) {
            var check = true
            for (index in value.indices) {
                if (value[index].equals('@')) check = true
                if (value[index].equals(' ')) {
                    check = false
                    break
                }
            }
            if (check) field = value
        }
    var schoolarship: Boolean = false
        set(value) {
            if (averagePoint >= 8) schoolarship = true
        }

    override fun inputInfo() {
        super.inputInfo()
        println("id: ")
        this.id = readlnOrNull().toString()
        println("Name: ")
        this.name = readlnOrNull().toString()
    }

    override fun showInfo() {
        super.showInfo()
        println(", id: $id, average point: $averagePoint, email: $email.")
    }
}

